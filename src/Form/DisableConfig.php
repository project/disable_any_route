<?php

namespace Drupal\disable_any_route\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration to disable/enable user login and registration.
 */
class DisableConfig extends ConfigFormBase {

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Oauth2TokenSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param string[] $claim_names
   *   The names of the claims.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * Creates the form.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \Drupal\simple_oauth\Form\OpenIdConnectSettingsForm
   *   The form.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['disable_any_route.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disable_any_route';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $data = \Drupal::cache()->get('disable_any_route.data');
    $config = $this->config('disable_any_route.settings');
    $disabled = $config->get('disabled');
    $default = [];
    $header = [
      'title' => t('Title'),
      'key' => t('Key'),
    ];
    $output = [];
    foreach ($data->data as $key => $title) {
      $output[$key]['title']['data']['#markup'] = '<span class="table-filter-text-source">' . $title . '</span>';
      $output[$key]['key']['data']['#markup'] = '<span class="module-name">' . $key . '</span>';
      $default[$key] = in_array($key, $disabled);
    }
    $form['#attached']['library'][] = 'system/drupal.system.modules';

    $form['filters'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['table-filter', 'js-show'],
      ],
    ];
    $form['filters']['title'] = [
      '#type' => 'search',
      '#title' => $this->t('Search'),
      '#size' => 30,
      '#placeholder' => $this->t('Enter title'),
      '#attributes' => [
        'class' => ['table-filter-text'],
        'data-table' => '#route-filter-text',
        'autocomplete' => 'off',
        'title' => $this->t('Enter a part of the name to filter by.'),
      ],
    ];

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $output,
      '#empty' => $this->t('No users found'),
      '#sticky' => TRUE,
      '#default_value' => $default,
      '#attributes' => [
        'id' => 'route-filter-text',
        'class' => ['route-parameter-list'],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tables = $form_state->getValue('table');
    $values = array_values($tables);
    $values = array_filter($values, function ($value) {
      return !empty($value);
    });
    $config = $this->config('disable_any_route.settings');
    $config->set('disabled', $values);
    $config->save();

    $this->messenger()
      ->addStatus('Your configuration has been saved, Please clear cache to apply.');
    parent::submitForm($form, $form_state);
  }

}
