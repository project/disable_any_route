<?php

namespace Drupal\disable_any_route\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $config = \Drupal::config('disable_any_route.settings');
    $disabled = $config->get('disabled');
    if (!empty($disabled)) {
      foreach ($disabled as $key) {
        if ($route = $collection->get($key)) {
          $route->setRequirement('_role', 'administrator');
        }
      }
    }

    $data = [];
    foreach ($collection->all() as $key => $route) {
      $default = $route->getDefaults();
      $data[$key] = !empty($default['_title']) ? $default['_title'] : $key;
    }
    \Drupal::cache()->set('disable_any_route.data', $data);
  }

}
